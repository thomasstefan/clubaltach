package at.fhv.itm19.st.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Club.
 */
@Entity
@Table(name = "club")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Club implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "vereins_name", nullable = false)
    private String vereinsName;

    @OneToMany(mappedBy = "club")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Team> teams = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVereinsName() {
        return vereinsName;
    }

    public Club vereinsName(String vereinsName) {
        this.vereinsName = vereinsName;
        return this;
    }

    public void setVereinsName(String vereinsName) {
        this.vereinsName = vereinsName;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public Club teams(Set<Team> teams) {
        this.teams = teams;
        return this;
    }

    public Club addTeam(Team team) {
        this.teams.add(team);
        team.setClub(this);
        return this;
    }

    public Club removeTeam(Team team) {
        this.teams.remove(team);
        team.setClub(null);
        return this;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Club)) {
            return false;
        }
        return id != null && id.equals(((Club) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Club{" +
            "id=" + getId() +
            ", vereinsName='" + getVereinsName() + "'" +
            "}";
    }
}
