import { IPlayer } from 'app/shared/model/player.model';
import { IMatchResult } from 'app/shared/model/match-result.model';
import { IClub } from 'app/shared/model/club.model';

export interface ITeam {
  id?: number;
  teamName?: string;
  players?: IPlayer[];
  matchResults?: IMatchResult[];
  club?: IClub;
}

export const defaultValue: Readonly<ITeam> = {};
