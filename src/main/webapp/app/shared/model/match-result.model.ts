import { ITeam } from 'app/shared/model/team.model';

export interface IMatchResult {
  id?: number;
  opponent?: string;
  score?: string;
  team?: ITeam;
}

export const defaultValue: Readonly<IMatchResult> = {};
