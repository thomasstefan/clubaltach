import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IMatchResult, defaultValue } from 'app/shared/model/match-result.model';

export const ACTION_TYPES = {
  FETCH_MATCHRESULT_LIST: 'matchResult/FETCH_MATCHRESULT_LIST',
  FETCH_MATCHRESULT: 'matchResult/FETCH_MATCHRESULT',
  CREATE_MATCHRESULT: 'matchResult/CREATE_MATCHRESULT',
  UPDATE_MATCHRESULT: 'matchResult/UPDATE_MATCHRESULT',
  DELETE_MATCHRESULT: 'matchResult/DELETE_MATCHRESULT',
  RESET: 'matchResult/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IMatchResult>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type MatchResultState = Readonly<typeof initialState>;

// Reducer

export default (state: MatchResultState = initialState, action): MatchResultState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_MATCHRESULT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_MATCHRESULT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_MATCHRESULT):
    case REQUEST(ACTION_TYPES.UPDATE_MATCHRESULT):
    case REQUEST(ACTION_TYPES.DELETE_MATCHRESULT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_MATCHRESULT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_MATCHRESULT):
    case FAILURE(ACTION_TYPES.CREATE_MATCHRESULT):
    case FAILURE(ACTION_TYPES.UPDATE_MATCHRESULT):
    case FAILURE(ACTION_TYPES.DELETE_MATCHRESULT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_MATCHRESULT_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_MATCHRESULT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_MATCHRESULT):
    case SUCCESS(ACTION_TYPES.UPDATE_MATCHRESULT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_MATCHRESULT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/match-results';

// Actions

export const getEntities: ICrudGetAllAction<IMatchResult> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_MATCHRESULT_LIST,
    payload: axios.get<IMatchResult>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IMatchResult> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_MATCHRESULT,
    payload: axios.get<IMatchResult>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IMatchResult> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_MATCHRESULT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IMatchResult> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_MATCHRESULT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IMatchResult> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_MATCHRESULT,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
